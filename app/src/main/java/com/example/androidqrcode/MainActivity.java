package com.example.androidqrcode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    Button btnClick;
    TextView txtName, txtDiaChi;
    ImageView imgHinh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnClick = (Button) findViewById(R.id.btnClick);
        imgHinh = (ImageView) findViewById(R.id.imgQRCode);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDiaChi = (TextView) findViewById(R.id.txtDiaChi);

        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentIntegrator.initiateScan();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null){
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }else {
                Picasso.get().load(result.getContents()).into(imgHinh);
                try {
                    JSONObject jsonObject = new JSONObject(result.getContents());
                    txtDiaChi.setText(jsonObject.getString("DiaChi"));
                    txtName.setText(jsonObject.getString("Name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else {
            super.onActivityResult(requestCode, resultCode,data);
        }
    }



}